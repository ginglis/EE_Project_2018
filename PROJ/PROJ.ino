#include <LiquidCrystal.h> // LCD library

LiquidCrystal lcd(11, 12, 4, 5, 6, 7); // initialize the LCD

// ints declared volatile so they can be modified from click()
volatile int seconds = -01;
volatile int minutes = 00;
volatile int hours = 00;

void setup() {
  lcd.begin(16,2); // 16x2 LCD 
  pinMode(2,INPUT); 
  attachInterrupt(0,click,RISING); // attaches pin 2 as an interrupt that executes click() when voltage goes from 0V to 5V
  // Starting the timer at time=00:00:00
  lcd.setCursor(0,1);
  lcd.print("Timer: ");
  lcd.setCursor(6,1);
  lcd.print("00");
  lcd.setCursor(8,1);
  lcd.print(":");
  lcd.setCursor(9,1);
  lcd.print("00");
  lcd.setCursor(11,1);
  lcd.print(":");
  lcd.setCursor(12,1);
  lcd.print("00");
}

void loop() {
 
seconds++; // increase timer

if(seconds < 10){
    lcd.setCursor(12,1); // set cursor to tens position
    lcd.print("0 "); // tens position is 0
    lcd.setCursor(13,1); // set cursor to ones position
    lcd.print(seconds); // display value of seconds to LCD
    
  }
  else if(seconds >=10 && seconds < 60){ 
    lcd.setCursor(12,1); // set cursor to tens position
    lcd.print(seconds); // display value of seconds to LCD
   
  }
  else if(seconds >= 60){
    seconds=-00; // reset seconds
    minutes++; // increase value of minutes when 60s is reached
    lcd.setCursor(12,1);
    lcd.print("0"); 
    lcd.print(seconds); 
  }
  
  if(minutes < 10){
    lcd.setCursor(9,1);
    lcd.print("0 ");
    lcd.setCursor(10,1);
    lcd.print(minutes);
    delay(1000); // delay included to account for increase in seconds
  }
  else if(minutes >=10 && minutes < 60){ 
    lcd.setCursor(9,1);
    lcd.print(minutes);
    delay(1000);
  }
  else if(minutes >= 60){
    minutes=-00; // reset minutes
    hours++; // increase value of hours when 60m is reached
    lcd.setCursor(7,1);
    lcd.print(seconds);
    
  }
   if(hours < 10){
    lcd.setCursor(6,1);
    lcd.print("0 ");
    lcd.setCursor(7,1);
    lcd.print(hours);
  }
  else if(hours >=10 && hours < 60){ 
    lcd.setCursor(6,1);
    lcd.print(hours);  
}
}

void click() {
  // reset LCD
  seconds=-00;
  minutes=00;
  hours=00;
  lcd.setCursor(7,1);
  lcd.print("Drip Rate: ");
  lcd.setCursor(0,1);
  lcd.print("Timer: ");
  lcd.setCursor(6,1);
  lcd.print("00");v
  lcd.setCursor(8,1);
  lcd.print(":");
  lcd.setCursor(9,1);
  lcd.print("00");
  lcd.setCursor(11,1);
  lcd.print(":");
  lcd.setCursor(12,1);
  lcd.print("00");
  
}

