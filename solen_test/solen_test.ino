#include <LiquidCrystal.h> // initialize LCD

LiquidCrystal lcd(11, 12, 4, 5, 6, 7); // initialize LCD

int solpin = 9; // define pin for MOSFET to solenoid

void setup() {
  lcd.begin(16,2); // initialize LCD
  lcd.print("Drip Rate: "); // prints the drip rate
  pinMode(solpin, OUTPUT); // set solpin as an output
}

void loop() {
  analogWrite(10, 117); // set the screen brightness
  lcd.setCursor(0,1); // set cursor to second row
  solenoid(); // main function to change delay based on pot value
}

void solenoid() {
  int potval = analogRead(A0); // reads the value from the potentiometer at pin A0
  if (potval <= 1*1023/4) { // first quadrant of potvals
    lcd.print("                "); // clear LCD
    lcd.setCursor(0,1); // set cursor
    lcd.print("Slow:5s Delay"); // displays drip speed
    digitalWrite(9, HIGH); // opens the valve
    delay(1000); // open for 1000ms
    digitalWrite(9,LOW); // closes the valve
    delay(5000); // closed for 5000ms
  }
  
  // the above section's comments apply to each of the following conditionals as well

  else if (potval > 1*1023/4 && potval <= 2*1023/4) { // second quadrant of potvals
    lcd.print("                ");
    lcd.setCursor(0,1);
    lcd.print("Medium:3s Delay");
    digitalWrite(9, HIGH);
    delay(1000);
    digitalWrite(9,LOW);
    delay(3000);
  } 
  else if (potval > 2*1023/4 && potval <= 3*1023/4) { // third quadrant of potvals
    lcd.print("                ");
    lcd.setCursor(0,1);
    lcd.print("Fast:1s Delay");
    digitalWrite(9, HIGH);
    delay(1000);
    digitalWrite(9,LOW);
    delay(1000);
  } 
  else if (potval > 3*1023/4) { // fourth quadrant of potvals
    lcd.setCursor(0,1);
    lcd.print("Fastest:No Delay");
    digitalWrite(9, HIGH);
  }
}

